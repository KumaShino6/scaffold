> AIOS 应用脚手架，基于SpingBoot、Mybatis，使用gradle构建

### 具体操作

1. 将脚手架的文件放置到工程文件夹下
2. 设置你的模块名: 修改 `setting.gradle` 中的 `rootProject.name = "xxx-xxx-xxx"`
3. 仔细check下`build.gradle`：
    - mainClassName
4. 执行命令: `gradle -PpackageName=com.h3c.aios.xxx.xxx createJavaProject` **填写自己的package名**
5. 编写你的业务代码
